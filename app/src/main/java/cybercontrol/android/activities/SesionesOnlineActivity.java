package cybercontrol.android.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import cybercontrol.android.R;
import cybercontrol.android.adapters.SesionAdapter;
import cybercontrol.android.services.MyFirebaseMessagingService;

public class SesionesOnlineActivity extends AppCompatActivity {

    private static final String TAG = "SesionesOnlineActivity";
    static final String[] FRUITS = new String[] { "Apple", "Avocado", "Banana",
            "Blueberry", "Coconut", "Durian", "Guava", "Kiwifruit",
            "Jackfruit", "Mango", "Olive", "Pear", "Sugar-apple", "Apple", "Avocado", "Banana",
            "Blueberry", "Coconut", "Durian", "Guava", "Kiwifruit",
            "Jackfruit", "Mango", "Olive", "Pear", "Sugar-apple" };

    // Atributos
    ListView listView;
    SesionAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sesiones_online);
        MyFirebaseMessagingService.subscribe(MyFirebaseMessagingService.TEMA);

        this.setTitle("Sesiones en linea");

        listView = (ListView) findViewById(R.id.list1);

        adapter = new SesionAdapter(this);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // When clicked, show a toast with the TextView text
                Toast.makeText(getApplicationContext(),
                        ((TextView) view.findViewById(R.id.textUsuario)).getText(), Toast.LENGTH_SHORT).show();
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                Toast.makeText(getApplicationContext(),
                        "long click" + ((TextView) view.findViewById(R.id.textUsuario)).getText(), Toast.LENGTH_SHORT).show();
                return false;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sesiones_online, menu);

        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        System.out.println("actualizando..---");
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.i(TAG, " - - - - TOKEN: " + token );

        adapter.actualizarListado();
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        FirebaseMessaging.getInstance().unsubscribeFromTopic(MyFirebaseMessagingService.TEMA);
    }
}
