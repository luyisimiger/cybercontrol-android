package cybercontrol.android;

import cybercontrol.modelo.entidades.Sesion;

/**
 * Created by Luis Miguel on 6/15/2017.
 */

public class Servidor {

    //private static final String URL_BASE = "http://192.168.1.67:8084";
    //private static final String URL_PATH_SESIONES = "/cybercontrol/sesiones";

    private static final String URL_BASE = "http://192.168.1.109:8084";
    private static final String URL_PATH_SESIONES = "/cybercontrol/sesiones";

    public static String getHttpSesionListURL () {

        return URL_BASE
                + URL_PATH_SESIONES
                + "/online?type=json";

    }

    public static String getHttpSesionCloseURL (Sesion s) {

        return URL_BASE
                + URL_PATH_SESIONES
                + "/online/close?sesion.id=" + s.getId();

    }

}
