package cybercontrol.android.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import cybercontrol.android.R;
import cybercontrol.android.Servidor;
import cybercontrol.modelo.entidades.Sesion;
import cybercontrol.modelo.entidades.Usuario;

/**
 * Created by Luis Miguel on 6/6/2017.
 */

public class SesionAdapter extends ArrayAdapter {

    private static final String TAG = "SesionAdapter";

    private final Context context;
    private RequestQueue requestQueue;
    private JsonRequest jsonRequest;
    private List<Sesion> listaSesiones;

    static class ViewHolder {
        public TextView textUsuario;
        public TextView textSesion;
        public Button btnCerrar;
    }

    public SesionAdapter(Context context) {
        super(context, R.layout.sesion_item);
        this.context = context;

        setListaSesiones( new ArrayList<Sesion>() );

        // Crear nueva cola de peticiones
        requestQueue = Volley.newRequestQueue(context);

        actualizarListado();

    }

    public void setListaSesiones(List<Sesion> listaSesiones) {

        this.listaSesiones = listaSesiones;

        this.clear();
        this.addAll(listaSesiones);
        this.notifyDataSetChanged();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        ViewHolder viewHolder;

        // reuse views
        if (rowView == null) {

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            rowView = inflater.inflate(R.layout.sesion_item, parent, false);
            // configure view holder
            viewHolder = new ViewHolder();
            viewHolder.textUsuario = (TextView) rowView.findViewById(R.id.textUsuario);
            viewHolder.textSesion = (TextView) rowView.findViewById(R.id.textSesion);
            viewHolder.btnCerrar = (Button) rowView.findViewById(R.id.btnCerrar);
            rowView.setTag(viewHolder);
        }

        // fill data
        viewHolder = (ViewHolder) rowView.getTag();
        final Sesion sesion = listaSesiones.get(position);

        if (sesion != null) {
            viewHolder.textUsuario.setText(sesion.getUsuarioId().getNombre());
            viewHolder.textSesion.setText("Sesion # " + sesion.getId());
        } else {
            viewHolder.textUsuario.setText("---");
            viewHolder.textSesion.setText("---");
        }

        // set handler
        viewHolder.btnCerrar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.i(TAG, "clicked btn: " + sesion.getId());

                // se crea el dialogo de confirmacion
                AlertDialog.Builder builder = new AlertDialog.Builder( SesionAdapter.this.getContext() );
                builder.setTitle(R.string.dialog_sesionCerrar_title)
                    .setMessage(R.string.dialog_sesionCerrar_message)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            JSONObject jsonData = null;
                            Response.Listener listener = new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.i(TAG, response);
                                }
                            };
                            Response.ErrorListener errorListener = null;

                            StringRequest request = new StringRequest(
                                    Request.Method.GET,
                                    Servidor.getHttpSesionCloseURL(sesion),
                                    listener,
                                    errorListener

                            );

                            Log.i(TAG, request.toString());

                            // agregar request a la cola
                            requestQueue.add(request);

                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });

                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });

        return rowView;
    }

    public List<Sesion> parseJson(JSONObject jsonObject){

        List<Sesion> lista = new ArrayList<Sesion>();

        try {

            // Obtener el array del objeto
            JSONArray jsonArray = jsonObject.getJSONArray("listaSesiones");

            for(int i=0; i<jsonArray.length(); i++){

                try {

                    JSONObject jsonSesion = jsonArray.getJSONObject(i);

                    // TODO: implementar uso de GSON API
                    Sesion sesion = Sesion.loadFromJSON(jsonSesion);

                    lista.add(sesion);

                } catch (JSONException e) {
                    Log.e(TAG, "Error de parsing: " + e.getMessage());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return lista;
    }

    public void actualizarListado() {

        // Nueva petición JSONObject
        jsonRequest = new JsonObjectRequest(
                Request.Method.GET,
                Servidor.getHttpSesionListURL(),
                null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "respuesta recibida del servidor ***************");
                        setListaSesiones( parseJson(response) );


                        if (listaSesiones.size() > 0) {
                            Log.i(TAG, listaSesiones.get(0).getUsuarioId().getNombre());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "Error Respuesta en JSON: " + error.getMessage());

                    }
                }
        );

        // Añadir petición a la cola
        requestQueue.add(jsonRequest);

    }
}
