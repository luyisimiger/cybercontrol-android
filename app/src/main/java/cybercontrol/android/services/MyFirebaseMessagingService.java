package cybercontrol.android.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import cybercontrol.android.R;
import cybercontrol.android.activities.SesionesOnlineActivity;

/**
 * Created by Luis Miguel on 6/1/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String TEMA = "newSesion";
    private static final String TAG = "MyFirebaseCloudService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "¡Mensaje recibido!");
        Log.d(TAG, "From:" + remoteMessage.getFrom());
        Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        Log.d(TAG, "notificacion.title: " + remoteMessage.getNotification().getTitle());
        Log.d(TAG, "notificacion.body: " + remoteMessage.getNotification().getBody());


        // TODO: procesar mensajes que llegan

        displayNotification(remoteMessage.getNotification(), remoteMessage.getData());
        //sendNewPromoBroadcast(remoteMessage);

    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public static void subscribe(String tema) {
        FirebaseMessaging.getInstance().subscribeToTopic(tema);
        Log.i(TAG, "suscristo al tema: " + tema);
    }

    public static void unsubscribe(String tema) {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(tema);
        Log.i(TAG, "de-suscristo al tema: " + tema);
    }

    private void displayNotification(RemoteMessage.Notification notification, Map<String, String> data) {

        Intent intent = new Intent(this, SesionesOnlineActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                //.setSmallIcon(R.drawable.ic_car)
                .setContentTitle(notification.getTitle())
                .setContentText(notification.getBody())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setContentInfo(notification.getTitle());
                //.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)


        notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

}
