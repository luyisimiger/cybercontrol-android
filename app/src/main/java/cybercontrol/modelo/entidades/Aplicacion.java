/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cybercontrol.modelo.entidades;

import java.io.Serializable;

/**
 *
 * @author Luis Miguel
 */
public class Aplicacion implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String nombreApp;
    private int pid;
    private Sesion sesionId;

    public Aplicacion() {
    }

    public Aplicacion(Integer id) {
        this.id = id;
    }

    public Aplicacion(Integer id, String nombreApp, int pid) {
        this.id = id;
        this.nombreApp = nombreApp;
        this.pid = pid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombreApp() {
        return nombreApp;
    }

    public void setNombreApp(String nombreApp) {
        this.nombreApp = nombreApp;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public Sesion getSesionId() {
        return sesionId;
    }

    public void setSesionId(Sesion sesionId) {
        this.sesionId = sesionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Aplicacion)) {
            return false;
        }
        Aplicacion other = (Aplicacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cybercontrol.modelo.entidades.Aplicacion[ id=" + id + " ]";
    }

}
