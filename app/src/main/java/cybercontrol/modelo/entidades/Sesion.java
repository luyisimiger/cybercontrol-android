/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cybercontrol.modelo.entidades;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Luis Miguel
 */
public class Sesion implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private Date fechaInicio;
    private Date fechaCierre;
    private String estado;
    private String ipCliente;
    private Usuario usuarioId;
    private List<Aplicacion> aplicacionList;

    public Sesion() {
    }

    public Sesion(Integer id) {
        this.id = id;
    }

    public Sesion(Integer id, Date fechaInicio, String estado, String ipCliente) {
        this.id = id;
        this.fechaInicio = fechaInicio;
        this.estado = estado;
        this.ipCliente = ipCliente;
    }

    public static Sesion loadFromJSON(JSONObject objeto) throws JSONException {

        Sesion sesion = new Sesion();
        sesion.setId( objeto.getInt("id") );
        sesion.setIpCliente( objeto.getString("ipCliente") );

        JSONObject jsonUsuario = objeto.getJSONObject("usuarioId");
        sesion.setUsuarioId( Usuario.loadFromJSON(jsonUsuario) );

        return sesion;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(Date fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIpCliente() {
        return ipCliente;
    }

    public void setIpCliente(String ipCliente) {
        this.ipCliente = ipCliente;
    }

    public Usuario getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Usuario usuarioId) {
        this.usuarioId = usuarioId;
    }

    public List<Aplicacion> getAplicacionList() {
        return aplicacionList;
    }

    public void setAplicacionList(List<Aplicacion> aplicacionList) {
        this.aplicacionList = aplicacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Sesion)) {
            return false;
        }
        Sesion other = (Sesion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cybercontrol.modelo.entidades.Sesion[ id=" + id + " ]";
    }

}
