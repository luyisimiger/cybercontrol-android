/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cybercontrol.modelo.entidades;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Luis Miguel
 */
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String username;
    private String password;
    private String nombre;
    private String tipo;
    private int tiempoDisponible;

    private List<Sesion> sesionList;

    public Usuario() {
    }

    public Usuario(Integer id) {
        this.id = id;
    }

    public Usuario(Integer id, String username, String password, String nombre, String tipo, int tiempoDisponible) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.nombre = nombre;
        this.tipo = tipo;
        this.tiempoDisponible = tiempoDisponible;
    }

    public static Usuario loadFromJSON(JSONObject objeto) throws JSONException {

        Usuario usuario = new Usuario();
        usuario.setId( objeto.getInt("id") );
        usuario.setUsername( objeto.getString("username") );
        usuario.setPassword( objeto.getString("password") );
        usuario.setNombre( objeto.getString("nombre") );
        usuario.setTipo( objeto.getString("tipo") );
        usuario.setTiempoDisponible( objeto.getInt("tiempoDisponible") );

        return usuario;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getTiempoDisponible() {
        return tiempoDisponible;
    }

    public void setTiempoDisponible(int tiempoDisponible) {
        this.tiempoDisponible = tiempoDisponible;
    }

    public List<Sesion> getSesionList() {
        return sesionList;
    }

    public void setSesionList(List<Sesion> sesionList) {
        this.sesionList = sesionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cybercontrol.modelo.entidades.Usuario[ id=" + id + " ]";
    }

}
